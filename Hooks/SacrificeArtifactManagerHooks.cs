﻿using CommandArtifactItemDrops.Utilities;
using On.RoR2.Artifacts;
using RoR2;

namespace CommandArtifactItemDrops.Hooks
{
    /// <summary>
    /// The sacrifice artifact manager hooks
    /// </summary>
    public class SacrificeArtifactManagerHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            SacrificeArtifactManager.OnServerCharacterDeath += SacrificeArtifactManager_OnOnServerCharacterDeath;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            SacrificeArtifactManager.OnServerCharacterDeath -= SacrificeArtifactManager_OnOnServerCharacterDeath;
        }

        private static void SacrificeArtifactManager_OnOnServerCharacterDeath(
            SacrificeArtifactManager.orig_OnServerCharacterDeath orig,
            DamageReport damageReport)
        {
            // roll to see if check is successful
            if (CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageFromSacrificeArtifact, true))
            {
                // enable the command artifact
                CommandArtifactHelper.ToggleCommandArtifact(true);
                CommandArtifactItemDropsPlugin.Logger.LogMessage($"Command artifact temporarily enabled after slaying {damageReport.victimBody.name} with {CommandArtifactItemDropsPlugin.DropChancePercentageFromSacrificeArtifact.Value:#.000}% chance!");
            }

            // invoke the original method
            orig.Invoke(damageReport);

            // disable the command artifact
            CommandArtifactHelper.ToggleCommandArtifact(false);
        }
    }
}
