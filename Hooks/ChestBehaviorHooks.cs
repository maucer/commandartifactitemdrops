﻿using CommandArtifactItemDrops.Utilities;
using ChestBehavior = On.RoR2.ChestBehavior;

namespace CommandArtifactItemDrops.Hooks
{
    /// <summary>
    /// Handles event hooks related to chest behaviors
    /// </summary>
    public class ChestBehaviorHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            ChestBehavior.Open += ChestBehavior_OnOpen;
            ChestBehavior.ItemDrop += ChestBehavior_OnItemDrop;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            ChestBehavior.Open -= ChestBehavior_OnOpen;
            ChestBehavior.ItemDrop -= ChestBehavior_OnItemDrop;
        }

        /// <summary>
        /// Handles the chest behavior after a chest or similar container is opened
        /// </summary>
        /// <param name="orig">The original event</param>
        /// <param name="self">The object that triggered the event</param>
        private static void ChestBehavior_OnOpen(
            ChestBehavior.orig_Open orig, 
            RoR2.ChestBehavior self)
        {
            if (!CommandArtifactHelper.IsInBazaar())
            {
                // get the name of the drop container
                var dropContainer = self.gameObject.name.ToLower();

                // prevent the drop if the drop container was not from a chest
                if (IsChestType(dropContainer))
                {
                    // only drop the item chooser if the proc coefficient is successful
                    if (CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageFromChests))
                    {
                        // enable the command artifact
                        CommandArtifactHelper.ToggleCommandArtifact(true);
                        CommandArtifactItemDropsPlugin.Logger.LogMessage($"A command artifact item picker dropped from {dropContainer} with {CommandArtifactItemDropsPlugin.DropChancePercentageFromChests.Value:#.000}% chance!");
                    }
                }
            }

            // call the original event handler to continue its execution
            orig.Invoke(self);
        }

        /// <summary>
        /// Handles the chest behavior after an item is dropped from a chest
        /// </summary>
        /// <param name="orig">The original event</param>
        /// <param name="self">The object that triggered the event</param>
        private static void ChestBehavior_OnItemDrop(
            ChestBehavior.orig_ItemDrop orig, 
            RoR2.ChestBehavior self)
        {
            // call the original event handler to continue its execution
            orig.Invoke(self);

            // disable the command artifact after the item has dropped
            CommandArtifactHelper.ToggleCommandArtifact(false);
        }

        /// <summary>
        /// Returns true if the drop container was a type of chest
        /// </summary>
        /// <param name="dropContainer">The name of the drop container</param>
        /// <returns>True or false</returns>
        private static bool IsChestType(string dropContainer)
        {
            return dropContainer.Contains("chest")
                   || dropContainer.Contains("lunar")
                   || dropContainer.Contains("equipment");
        }
    }
}
