﻿using System.Collections.Generic;
using CommandArtifactItemDrops.Utilities;
using RoR2;
using UnityEngine;
using DeathRewards = On.RoR2.DeathRewards;

namespace CommandArtifactItemDrops.Hooks
{
    public class KillRewardHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            DeathRewards.OnKilledServer += DeathRewards_OnOnKilledServer;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            DeathRewards.OnKilledServer -= DeathRewards_OnOnKilledServer;
        }

        private static void DeathRewards_OnOnKilledServer(
            DeathRewards.orig_OnKilledServer orig,
            RoR2.DeathRewards self,
            DamageReport damageReport)
        {
            // do not do anything if the slain enemy was not a monster type
            if (damageReport?.victimBody == null
                || damageReport.victimBody.teamComponent.teamIndex != TeamIndex.Monster)
            {
                // perform the original action
                orig.Invoke(self, damageReport);

                return;
            }

            // only drop the item if the proc coefficient is successful
            if (CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageForKillRewards, true))
            {
                var isKillRewardAnItemPicker = CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageForKillRewardItemPicker);
                if (isKillRewardAnItemPicker)
                {
                    // only drop the item chooser if the proc coefficient is successful
                    CommandArtifactHelper.ToggleCommandArtifact(true);
                }

                // drop the kill reward
                var itemRarity = DropRandomItem(damageReport);

                // ensure the command artifact is disabled
                CommandArtifactHelper.ToggleCommandArtifact(false);

                // inform the user
                var killRewardsPerMinute = ++CommandArtifactItemDropsPlugin.TotalKillRewards / Run.instance.GetRunStopwatch();
                CommandArtifactItemDropsPlugin.Logger.LogMessage($"{itemRarity} {(isKillRewardAnItemPicker ? "command artifact item picker " : string.Empty)}kill reward has dropped after slaying {damageReport.victimBody.name} with {CommandArtifactItemDropsPlugin.DropChancePercentageForKillRewards.Value:#.000}% chance!");
                CommandArtifactItemDropsPlugin.Logger.LogInfo($"Total kill rewards: {CommandArtifactItemDropsPlugin.TotalKillRewards} [{killRewardsPerMinute:#.00000} per minute]");
            }
            else
            {
                // perform the original action
                orig.Invoke(self, damageReport);
            }
        }

        private static string DropRandomItem(
            DamageReport damageReport)
        {
            List<PickupIndex> dropList;
            var itemRarity = "A common";

            // check to see which tier of item will be rewarded
            if (CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageForKillRewardRare))
            {
                // a rare item was rewarded
                dropList = Run.instance.availableTier3DropList;
                itemRarity = "A rare";
            }
            else if (CommandArtifactHelper.IsCheckSuccessful(CommandArtifactItemDropsPlugin.DropChancePercentageForKillRewardUncommon))
            {
                // an uncommon item was rewarded
                dropList = Run.instance.availableTier2DropList;
                itemRarity = "An uncommon";
            }
            else
            {
                // a common item was rewarded
                dropList = Run.instance.availableTier1DropList;
            }

            // get a random item from the drop list
            var randomItem = dropList[CommandArtifactItemDropsPlugin.Random.Next(0, dropList.Count)];

            // drop the item
            PickupDropletController.CreatePickupDroplet(randomItem, damageReport.victimBody.transform.position, new Vector3(UnityEngine.Random.Range(-5.0f, 5.0f), 20f, UnityEngine.Random.Range(-5.0f, 5.0f)));

            return itemRarity;
        }
    }
}
