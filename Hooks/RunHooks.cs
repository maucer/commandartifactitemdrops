﻿using CommandArtifactItemDrops.Utilities;
using On.RoR2;

namespace CommandArtifactItemDrops.Hooks
{
    public class RunHooks
    {
        /// <summary>
        /// Binds the event hooks
        /// </summary>
        public static void Bind()
        {
            Unbind();

            Run.Start += RunOnStart;
        }

        /// <summary>
        /// Unbinds the event hooks
        /// </summary>
        private static void Unbind()
        {
            Run.Start -= RunOnStart;
        }

        private static void RunOnStart(
            Run.orig_Start orig, 
            RoR2.Run self)
        {
            CommandArtifactHelper.ResetGameStatistics();

            orig.Invoke(self);
        }
    }
}
