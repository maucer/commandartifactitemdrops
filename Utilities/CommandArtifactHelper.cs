﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BepInEx.Configuration;
using CommandArtifactItemDrops.Enums;
using R2API;
using RoR2;
using UnityEngine.SceneManagement;

namespace CommandArtifactItemDrops.Utilities
{
    /// <summary>
    /// The command artifact helper
    /// </summary>
    public static class CommandArtifactHelper
    {
        public static bool IsInBazaar()
        {
            var isInBazaar = SceneManager.GetActiveScene().name.Equals(DirectorAPI.Stage.Bazaar.ToString(), StringComparison.InvariantCultureIgnoreCase);
            return isInBazaar;
        }

        /// <summary>
        /// Toggles the enabled status of the command artifact
        /// </summary>
        /// <param name="isEnabled">A value indicating whether the command artifact
        /// will be enabled or disabled</param>
        public static void ToggleCommandArtifact(bool isEnabled)
        {
            // get the command artifact definition
            var commandArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Command));

            if (RunArtifactManager.instance.IsArtifactEnabled(commandArtifact) == isEnabled)
            {
                // artifact status is not changing
                return;
            }

            if (isEnabled)
            {
                // enable the command artifact 
                RunArtifactManager.instance.SetArtifactEnabledServer(commandArtifact, true);
            }
            else
            {
                Task.Run(() =>
                {
                    // sleep for a short time before disabling the command artifact
                    Thread.Sleep(CommandArtifactItemDropsPlugin.CommandArtifactTimeout);

                    // disable the command artifact 
                    RunArtifactManager.instance.SetArtifactEnabledServer(commandArtifact, false);
                });
            }
        }

        /// <summary>
        /// Returns whether a single check of a random drop coefficient is successful against the configuration
        /// entity's drop chance percentage
        /// </summary>
        /// <param name="configEntry">The configuration entity</param>
        /// <param name="isInfluencedBySwarmsArtifact">A value indicating whether the drop chance should be
        /// reduced by half when the Swarms artifact is enabled</param>
        /// <returns>Returns true if the check is successful, otherwise false</returns>
        public static bool IsCheckSuccessful(ConfigEntry<float> configEntry, bool isInfluencedBySwarmsArtifact = false)
        {
            // safe-guard the configurable drop chance percentage
            var dropChancePercent = configEntry.Value;
            if (dropChancePercent < 0 || dropChancePercent > 100)
            {
                dropChancePercent = Convert.ToSingle(configEntry.DefaultValue);
            }

            if (isInfluencedBySwarmsArtifact)
            {
                var swarmsArtifact = ArtifactCatalog.FindArtifactDef(nameof(ArtifactEnum.Swarms));
                if (RunArtifactManager.instance.IsArtifactEnabled(swarmsArtifact))
                {
                    // cut the drop rate in half if swarms is enabled
                    dropChancePercent /= 2;
                }
            }

            // only drop the item chooser if the proc coefficient is successful
            var dropCoefficient = CommandArtifactItemDropsPlugin.Random.NextDouble() * 100;
            if (dropCoefficient < dropChancePercent)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Resets the internal counts for drop rewards
        /// </summary>
        public static void ResetGameStatistics()
        {
            CommandArtifactItemDropsPlugin.Logger.LogInfo("Resetting run statistics");
            CommandArtifactItemDropsPlugin.TotalKillRewards = 0;
        }
    }
}
