﻿using BepInEx;
using BepInEx.Configuration;
using BepInEx.Logging;
using CommandArtifactItemDrops.Hooks;
using R2API;
using R2API.Utils;
using Random = System.Random;

namespace CommandArtifactItemDrops
{
    /// <summary>
    /// The Command Artifact Item Drops plugin mod for Risk of Rain 2
    /// </summary>
    [BepInDependency(R2API.R2API.PluginGUID)]
    [R2APISubmoduleDependency(nameof(ItemAPI), nameof(ItemDropAPI), nameof(ResourcesAPI))]
    [BepInPlugin(ModGuid, ModName, ModVer)]
    [NetworkCompatibility(CompatibilityLevel.NoNeedForSync)]
    public class CommandArtifactItemDropsPlugin : BaseUnityPlugin
    {
        private const string ModGuid = "com.maucer.CommandArtifactItemDrops";
        private const string ModName = "Command Artifact Item Drops";
        private const string ModVer = "1.0.1";

        private const float DefaultChanceFromChests = 5.0f;
        private const float DefaultChanceFromSacrificeArtifact = 5.0f;
        private const float DefaultChanceForKillRewards = 0.1f;
        private const float DefaultChanceForKillRewardItemPicker = 5.0f;
        private const float DefaultChanceForKillRewardUncommon = 30.0f;
        private const float DefaultChanceForKillRewardRare = 5.0f;

        internal static double TotalKillRewards = 0;


        /// <summary>
        /// The internal logging source for this plugin
        /// </summary>
        internal new static ManualLogSource Logger;

        /// <summary>
        /// The random number generator used for determining whether a drop chance is successful
        /// </summary>
        internal static Random Random;

        /// <summary>
        /// The time in milliseconds that the Command Artifact will be enabled during a successful drop
        /// </summary>
        internal static int CommandArtifactTimeout = 1000;

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// a chest object
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageFromChests { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// killing a monster while the sacrifice artifact is enabled
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageFromSacrificeArtifact { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a random item to drop as a death reward
        /// after killing a monster 
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageForKillRewards { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a single item picker drop from
        /// killing a monster from death rewards while the sacrifice artifact is enabled
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageForKillRewardItemPicker { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a chest dropped from death rewards to
        /// be of uncommon rarity
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageForKillRewardUncommon { get; set; }

        /// <summary>
        /// The configured drop chance percentage for a chest dropped from death rewards to
        /// be of rare rarity
        /// </summary>
        internal static ConfigEntry<float> DropChancePercentageForKillRewardRare { get; set; }

        public void Awake()
        {
            Logger = base.Logger;
            Random = new Random();

            LoadConfigurationSettings(Config);
            InitializeEventHooks();

            On.RoR2.Console.Awake += (orig, self) =>
            {
                CommandHelper.AddToConsoleWhenReady();
                orig(self);
            };
        }

        /// <summary>
        /// Loads the configuration settings 
        /// </summary>
        /// <param name="config"></param>
        private static void LoadConfigurationSettings(ConfigFile config)
        {
            // get the configured drop chance from chests
            DropChancePercentageFromChests = config.Bind(
                "Chests",
                "Drop Chance Percentage",
                DefaultChanceFromChests,
                $"Sets the chance that a command artifact item picker will drop from chests. [0.0-100.0%, default={DefaultChanceFromChests:#.00}%]");

            // get the configured drop chance from the sacrifice artifact
            DropChancePercentageFromSacrificeArtifact = config.Bind(
                "Sacrifice Artifact",
                "Drop Upgrade Chance Percentage",
                DefaultChanceFromSacrificeArtifact,
                $"Sets the chance that the drop from a slain monster will upgrade to a command artifact item picker when the sacrifice artifact is enabled and a drop occurs. [0.0-100.0%, default={DefaultChanceFromSacrificeArtifact:#.00}%]");

            // get the configured drop chance for death rewards
            DropChancePercentageForKillRewards = config.Bind(
                "Kill Rewards",
                "Drop Chance Percentage for Kill Rewards",
                DefaultChanceForKillRewards,
                $"Sets the chance that a random item will drop from slain monsters as a kill reward. [0.0-100.0% default={DefaultChanceForKillRewardItemPicker:#.00}%]");

            // get the configured drop chance from death rewards
            DropChancePercentageForKillRewardItemPicker = config.Bind(
                "Kill Rewards",
                "Drop Chance Percentage for Item Pickers from Kill Rewards",
                DefaultChanceForKillRewardItemPicker,
                $"Sets the chance that an item dropped as a kill reward from slain monsters will be a command artifact item picker. [0.0-100.0% default={DefaultChanceForKillRewardItemPicker:#.00}%]");

            // get the configured drop chance from death rewards when sacrifice is enabled
            DropChancePercentageForKillRewardUncommon = config.Bind(
                "Kill Rewards",
                "Drop Chance Percentage for Uncommons from Kill Rewards",
                DefaultChanceForKillRewardUncommon,
                $"Sets the chance that an item dropped as a kill reward from slain monsters will be uncommon. [0.0-100.0% default={DefaultChanceForKillRewardUncommon:#.00}%]");

            // get the configured drop chance from death rewards when sacrifice is enabled
            DropChancePercentageForKillRewardRare = config.Bind(
                "Kill Rewards",
                "Drop Chance Percentage for Rares from Kill Rewards",
                DefaultChanceForKillRewardRare,
                $"Sets the chance that an item dropped as a kill reward from slain monsters will be rare. [0.0-100.0% default={DefaultChanceForKillRewardRare:#.00}%]");
        }

        /// <summary>
        /// Initializes the event hooks
        /// </summary>
        private static void InitializeEventHooks()
        {
            RunHooks.Bind();
            ChestBehaviorHooks.Bind();
            SacrificeArtifactManagerHooks.Bind();
            KillRewardHooks.Bind();
        }
    }
}
