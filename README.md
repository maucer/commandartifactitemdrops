﻿
# Command Artifact Item Drops

This mod adds a configurable chance for dropped items to be coverted into Command Artifact item pickers of the same type.  Now players can experience the power of the Command Artifact under their own terms without having it affect every single drop in a run.  Default settings are fairly balanced granting about a 5% proc chance for item pickers to be found when items drop.  Note: This mod does not require the Command Artifact to be enabled and functions completely on its own.

## Installation

Install with a mod manager or drop the dll into your BepInEx directory.

## Configuration Settings

 - Chests
	- Drop Chance Percentage - Sets the chance that a command artifact item picker will drop from chests. [0.0-100.0%, default=5.000%].

- Kill Rewards
	- Drop Chance Percentage for Kill Rewards - Sets the chance that a random item will drop from slain monsters as a kill reward. [0.0-100.0%, default=0.100%].
	- Drop Chance Percentage for Item Pickers from Kill Rewards - Sets the chance that an item dropped as a kill reward from slain monsters will be a command artifact item picker. [0.0-100.0%, default=5.000%].
	- Drop Chance Percentage for Uncommons from Kill Rewards - Sets the chance that an item dropped as a kill reward from slain monsters will be uncommon. [0.0-100.0%, default=30.000%].
	- Drop Chance Percentage for Rares from Kill Rewards - Sets the chance that an item dropped as a kill reward from slain monsters will be rare. [0.0-100.0%, default=5.000%].

- Sacrifice Artifact
	- Drop Upgrade Chance Percentage - Sets the chance that the drop from a slain monster will upgrade to a command artifact item picker when the sacrifice artifact is enabled and a drop occurs. [0.0-100.0%, default=5.000%].

## Version History
- 1.0.1
	- Corrects a timing bug

- 1.0.0
	- Updates the mod to support the anniversary update.
	- Adds support for configuring item drops from slain monsters, with possible upgrades to command artifact item pickers and uncommon or rare items (similar to Sacrifice Artifact)

- 0.3.1
	- Corrects a bug that occurred when using the Sacrifice artifact along with BiggerBazaar while inside the bazaar.

- 0.3.0
	- Adds support for use with the Sacrifice artifact.

- 0.2.0
	- Corrects an unintentional interaction with the Sacrifice Artifact.

- 0.1.0
	- Initial revision.
